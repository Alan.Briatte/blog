import {Component, Input, OnInit} from '@angular/core';
import {faTimes, faHeart, faHeartBroken, faShareAlt} from "@fortawesome/free-solid-svg-icons";
import {MatCardModule, MatCardTitle, MatCardHeader, MatCardActions, MatCardContent} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {NgxMasonryOptions} from "ngx-masonry";
import {Router} from "@angular/router";
import {PostService} from "../services/post.service";
import {Post} from "../models/post-model";

@Component({
  selector: 'app-post-list-card',
  templateUrl: './post-list-card.component.html',
  styleUrls: ['./post-list-card.component.less']
})

export class PostListCardComponent implements OnInit {

  // attributs
  @Input() post: any;
  @Input() idx: number;

  // icons
  faClose = faTimes;
  faLike = faHeart;
  faDislike = faHeartBroken;

  @Input() ratio: number;
  dislike: number;
  like: number;

  constructor(private router: Router, private postService: PostService) {
  }

  ngOnInit() {
    this.like = this.post.like || 0;
    this.dislike = this.post.dislike || 0;
    this.changeRatio();
    this.postService.emitPosts();
  }

  viewPost() {
    this.router.navigate(['/post', 'view', this.idx]);
  }

  onRemove() {
    this.postService.removePost(this.idx);
    this.postService.emitPosts();
  }

  onLike() {
    this.like +=1;
    this.postService.likePost(this.like,this.idx);
    this.changeRatio();
  }

  onDislike(){
    this.dislike +=1;
    this.postService.dislikePost(this.dislike,this.idx);
    this.changeRatio();
  }

  changeRatio() {
    this.ratio = Math.sign(this.like - this.dislike);
  }

}
