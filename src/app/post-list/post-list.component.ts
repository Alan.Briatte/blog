import {Component, OnInit} from '@angular/core';
import {Post} from "../models/post-model";
import {Subscription} from "rxjs";
import {PostService} from "../services/post.service";
import {Router} from "@angular/router";
import {MatCardModule, MatCardTitle, MatCardHeader, MatCardActions, MatCardContent} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import * as $ from 'jquery';
import {NgxMasonryOptions} from "ngx-masonry";
import {faWindowClose, faHeart, faHeartBroken, faShareAlt} from "@fortawesome/free-solid-svg-icons";


@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.less']
})
export class PostListComponent implements OnInit {

  public myOptions: NgxMasonryOptions = {
    transitionDuration: '0.8s',
    gutter: 10,
    originTop: true,
  };

  posts: Post[];
  postsSubscription: Subscription;

  constructor(private postService: PostService) {

  }

  ngOnInit() {
    this.postsSubscription = this.postService.postSubject.subscribe(
      (posts: Post[]) => {
        this.posts = posts;
      }
    );
    this.postService.emitPosts();
  }

  trackByFn(index, item) {
    return index;

  }
}
