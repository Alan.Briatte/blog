import {Component, OnInit} from '@angular/core';
import {AuthService} from "../services/auth.service";
import {PostService} from "../services/post.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.less']
})
export class EditUserComponent implements OnInit {

  userEditForm: FormGroup;
  private user;
  userSubscription: Subscription;

  imageChangedEvent: any[];
  croppedImage: any[];

  loading: any[];
  loadingSubscription: Subscription;

  private fileUrl: string;
  private fileIsUploading = false;
  private fileUploaded = false;


  constructor(private formBuilder: FormBuilder, private authService: AuthService, private postService: PostService) {
  }

  ngOnInit() {
    this.userSubscription = this.authService.userSubject.subscribe(
      (value: any[]) => {
        this.user = value;
        console.log(this.user);
        this.createForm();
      });
    this.loadingSubscription = this.postService.loadingSubject.subscribe(
      (value: any[]) => {
        this.loading = value;
      }
    );
    this.postService.emitLoading();
    this.authService.emitUser();

  }

  createForm() {


    this.userEditForm = this.formBuilder.group({
      firstname: [this.user ? this.user.displayName.split(' ')[0] : ''],
      lastname: [this.user ? this.user.displayName.split(' ')[1] : ''],
      mail: [this.user ? this.user.email : ''],
      currentPassword: ['', [Validators.pattern(/[0-9a-zA-Z]{6,}/)]],
      newPassword: ['', [Validators.pattern(/[0-9a-zA-Z]{6,}/)]],
      newPasswordConfirm: ['', [Validators.pattern(/[0-9a-zA-Z]{6,}/)]],
      avatar: [''],
    });
  }

  onSubmit() {
    console.log(this.croppedImage);
    const config = {
      firstname: this.userEditForm.get('firstname').value,
      lastname: this.userEditForm.get('lastname').value,
      mail: this.userEditForm.get('mail').value,
      currentPassword: this.userEditForm.get('currentPassword').value,
      newPassword: this.userEditForm.get('newPassword').value,
      newPasswordConfirm: this.userEditForm.get('newPasswordConfirm').value,
      avatar: this.croppedImage
    };


    if (this.user.displayName !== `${config.firstname} ${config.lastname}`) {
      this.user.updateProfile({
        displayName: `${config.firstname} ${config.lastname}`,
      }).then(() => {
        console.log('nom mis à jour');
      }).catch(err => {

      });
    }

    if (this.user.photoURL !== config.avatar) {
      this.user.updateProfile({
        photoURL: config.avatar
      }).then(() => {
        console.log('avatar mis à jour');
      }).catch(err => {

      });
    }


    if (config.mail !== this.user.email) {
      this.user.updateEmail(config.mail).then(() => {
      }).catch((error) => {
      })
    }

    if (config.currentPassword.length) {
      this.user.updatePassword(config.newPassword).then(() => {
        // Update successful.
      }).catch((error) => {
        // An error happened.
      });
    }

    this.authService.emitUser();
  }

  detectFile(e) {
    this.imageChangedEvent = e;
    this.onUploadFile(e.target.files[0]);
  }

  onUploadFile(file: File) {
    this.fileIsUploading = true;
    this.postService.uploadFile(file).then(
      (url: string) => {
        this.fileUrl = url;
        this.fileIsUploading = false;
        this.fileUploaded = true;
      }
    )
  }
  cropperReady(){
    console.log('crop ready');
  }
  imageLoaded(){
    console.log('loaded');
  }
  imageCropped(e){
    console.log('image cropped',e);
    this.croppedImage = e.base64;
  }
}
