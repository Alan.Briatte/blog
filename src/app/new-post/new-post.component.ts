import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {PostService} from "../services/post.service";
import {Post} from '../models/post-model';
import {Router} from "@angular/router";
import {Observable, Subscription} from "rxjs";

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.less']
})
export class NewPostComponent implements OnInit, OnDestroy {
  postForm: FormGroup;
  fileUrl: string;
  fileIsUploading = false;
  fileUploaded = false;

  loading: any[];
  loadingSubscription: Subscription;


  constructor(private formBuilder: FormBuilder, private postService: PostService, private router: Router) {
  }

  ngOnInit() {
    this.postForm = this.formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      content: ['', Validators.required],
    });


    this.loadingSubscription = this.postService.loadingSubject.subscribe(
      (value: any[]) => {
        this.loading = value;
      }
    );
    this.postService.emitLoading();
  }


  savePost() {
    const title = this.postForm.get('title').value;
    const author = this.postForm.get('author').value;
    const content = this.postForm.get('content').value;

    let newPost = new Post(title, author, content);

    if (this.fileUrl && this.fileUrl !== '') {
      newPost.photo = this.fileUrl;
    }


    this.postService.createNewPost(newPost);
    this.router.navigate(['/post'])
  }

  detectFiles(event) {
    this.onUploadFile(event.target.files[0]);
  }

  onUploadFile(file: File) {
    this.fileIsUploading = true;
    this.postService.uploadFile(file).then(
      (url: string) => {
        this.fileUrl = url;
        this.fileIsUploading = false;
        this.fileUploaded = true;
      }
    )
  }


  ngOnDestroy(): void {

    this.loadingSubscription.unsubscribe();

  }
}
