import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PostService} from '../services/post.service'
import {Post} from '../models/post-model';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.less']
})
export class SinglePostComponent implements OnInit {

  post: Post;
  id: number;


  constructor(private postService: PostService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {

    this.post = new Post('', '', '');
    this.id = this.route.snapshot.params['id'];
    this.postService.getSinglePost(+this.id).then(
      (post: Post) => {
        this.post = post;
      }
    )
  }

  goBack() {
    this.router.navigate(['/post'])
  }

}
