import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HeaderViewComponent} from './header-view/header-view.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDividerModule} from '@angular/material/divider';
import {MatFormFieldModule} from '@angular/material/form-field';
import {PostListComponent} from './post-list/post-list.component';
import {PostListViewComponent} from './post-list-view/post-list-view.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ImageCropperModule} from 'ngx-image-cropper';
import {NewPostComponent} from './new-post/new-post.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {PostService} from "./services/post.service";
import {AuthService} from "./services/auth.service";
import {MatCardModule, MatInputModule} from "@angular/material";
import {MatButtonModule} from "@angular/material";
import {NgxMasonryModule} from 'ngx-masonry';
import {CreateUserComponent} from './create-user/create-user.component';
import {SigninComponent} from './signin/signin.component';
import {AuthGuardService} from "./services/auth-guard.service";
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {FourOhFourComponent} from './four-oh-four/four-oh-four.component';
import {SinglePostComponent} from './single-post/single-post.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {PostListCardComponent} from './post-list-card/post-list-card.component';
import {EditUserComponent} from './edit-user/edit-user.component';


const appRoutes: Routes = [
  {path: 'post/new', canActivate: [AuthGuardService], component: NewPostComponent},
  {path: 'post/view/:id', canActivate: [AuthGuardService], component: SinglePostComponent},
  {path: 'post', canActivate: [AuthGuardService], component: PostListViewComponent},
  {path: 'auth/signup', component: CreateUserComponent},
  {path: 'auth/signin', component: SigninComponent},
  {path: 'auth/edit', component: EditUserComponent},
  {path: '', component: PostListViewComponent},
  {path: 'fourohfour', component: FourOhFourComponent},
  {path: '**', component: FourOhFourComponent, pathMatch: 'full'},
];


@NgModule({
  declarations: [
    AppComponent,
    HeaderViewComponent,
    PostListComponent,
    PostListViewComponent,
    NewPostComponent,
    CreateUserComponent,
    SigninComponent,
    FourOhFourComponent,
    SinglePostComponent,
    PostListCardComponent,
    EditUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTabsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatDividerModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    NgxMasonryModule,
    MatProgressSpinnerModule,
    FontAwesomeModule,
    ImageCropperModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [PostService, AuthService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule {
  private static authService: any;
}
