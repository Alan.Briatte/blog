import {Injectable} from '@angular/core';
import {Post} from "../models/post-model";
import {Subject} from "rxjs";
import * as firebase from "firebase";
import {Router} from '@angular/router';
import DataSnapshot = firebase.database.DataSnapshot;

@Injectable()
export class PostService {

  loadingSubject = new Subject<any[]>();
  private loading;


  posts: Post[] = [];
  postSubject = new Subject<Post[]>();


  constructor(private router: Router) {
    this.getPosts();
  }

  getPosts() {
    firebase.database().ref('/posts')
      .on('value', (data: DataSnapshot) => {
        this.posts = data.val() ? data.val() : [];
        this.emitPosts();
      })
  }

  emitPosts() {
    this.postSubject.next(this.posts);
  }

  emitLoading() {
    this.loadingSubject.next(this.loading);
  }

  getSinglePost(id) {
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/posts/' + id).once('value').then(
          (data: DataSnapshot) => {
            if (!data.val()) {
              this.router.navigate(['/fourohfour'])
            }
            resolve(data.val());
          }, err => {
            reject(err);
          }
        )
      }
    )
  }

  createNewPost(newPost: Post) {
    this.posts.push(newPost);
    firebase.database().ref('/posts').set(this.posts);
    this.emitPosts();
  }

  removePost(idx: number) {
    this.posts.splice(idx, 1);
    this.emitPosts();
    firebase.database().ref('/posts').set(this.posts);
  }

  likePost(like: number, i: number) {
    // this.posts[i].like = like;
    let ref = firebase.database().ref('posts/' + i + '/like');
    ref.set(like);
    // this.emitPosts();

  }

  dislikePost(dislike: number, i: number) {
    // this.posts[i].dislike = dislike;
    let ref = firebase.database().ref('posts/' + i + '/dislike');
    ref.set(dislike);
    // this.emitPosts();
  }


  uploadFile(file: File) {
    return new Promise(
      (resolve, reject) => {
        const almostUniqueFileName = Date.now().toString();

        const upload = firebase.storage().ref()
          .child('images/' + almostUniqueFileName + file.name).put(file);

        upload.on(firebase.storage.TaskEvent.STATE_CHANGED,
          (ev) => {
            let {bytesTransferred, totalBytes} = ev;
            this.loading = (bytesTransferred * 100) / totalBytes;
            this.emitLoading();

          },
          (error) => {
            console.log('Erreur de chargement ! : ');
            console.warn(error);

            reject();
          }, () => {
            resolve(upload.snapshot.ref.getDownloadURL());
          })
      }
    )
  }
}
