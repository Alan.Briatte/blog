import {Injectable} from '@angular/core';
import * as firebase from 'firebase';
import {promise} from "selenium-webdriver";
import {Router} from "@angular/router";
import {Subject} from "rxjs";

@Injectable()
export class AuthService {

  userSubject = new Subject<any[]>();
  private user;

  constructor(private router: Router) {
   this.getUserInfo();
  }

  emitUser() {
    this.userSubject.next(this.user)
  }

  createUser(mail: string, psw: string) {

    return new Promise(
      (resolve, reject) => {
        firebase.auth().createUserWithEmailAndPassword(mail, psw).then(() => {
            resolve();
          },
          err => {
            reject(err)
          }
        )
      }
    )
  }

  //connexion
  onSignIn(mail: string, psw: string) {
    return new Promise(
      (resolve, reject) => {
        firebase.auth().signInWithEmailAndPassword(mail, psw).then(
          () => {
            resolve();
          },
          err => {
            reject(err);
          }
        )
      }
    )
  }

  onLogOut() {
    firebase.auth().signOut();
    this.router.navigate(['/auth/signin'])
  }

  getUserInfo() {
    firebase.auth().onAuthStateChanged((user) =>{
      if (user) {
        this.user = user;
        this.emitUser();
      } else {
        return;
      }
    });
  }

}
