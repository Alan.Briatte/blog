import {Component, OnInit} from '@angular/core';
import {MatToolbarModule} from '@angular/material/toolbar';
import {AuthService} from "../services/auth.service";
import * as firebase from "firebase";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-header-view',
  templateUrl: './header-view.component.html',
  styleUrls: ['./header-view.component.less']
})
export class HeaderViewComponent implements OnInit {

  isAuth: boolean;
  user;
  private userSubscription: Subscription;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    firebase.auth().onAuthStateChanged(
      user => {
        user ? this.isAuth = true : this.isAuth = false;
      }
    );

    this.userSubscription = this.authService.userSubject.subscribe(
      (value: any[]) => {
        this.user = value;
      });
    this.authService.emitUser()
  }


  logOut() {
    this.authService.onLogOut();
    firebase.auth().onAuthStateChanged(
      user => {
        user ? this.isAuth = true : this.isAuth = false;
        this.user = user;
        this.authService.emitUser();
      }
    );
  }

}
